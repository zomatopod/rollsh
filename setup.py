import os
from setuptools import setup, find_packages

setup(
    name="rollsh",
    version="0.1-albatross",
    author="Lindy Meas",
    author_email="lmeas@thenullpointers.com",
    description="Rolls some dice.",
    long_description="Rolls some dice.",
    packages=find_packages(),
    install_requires=[
        "grako==3.8.1"
    ],
    entry_points={
        'console_scripts': [
            'rollsh = rollsh:run'
        ]
    }
)
