"""
This module provides utilities to parse `dice notation`_ and simulate
dice rolls. In dice notation, a dice roll is declared in the form of::

    [amount]d[faces]

``2d6`` would roll two six-sided dice. If the amount and/or faces are
omitted, then ``1`` and ``6`` is assumed, respectively. Thus, ``1d6``,
``d6``, ``1d``, and ``d`` are all valid and equivalent.

.. _`dice notation`: http://en.wikipedia.org/wiki/Dice_notation

The notation may contain basic algebra for addition, subtraction,
multiplication, and division. Proper order of operations is respected,
including parenthesized groupings. Dice rolls may be modified by other
dice rolls or concrete integers in this fashion::

    2d6+10
    13+3d5*3
    3*(2d6+10)
    (75+1d25)/(2-1d4)*3

Because the result of a given notation is always a whole number,
quotients are floored if they are fractional.

In addition to the standard arithmetic operators ``+ - * /``, the
characters ``×`` (Unicode code point U+00D7) and ``x`` (lowercase ASCII
"X") can be used for multiplication, and ``÷`` for division.

"""

from rollsh.dice.notation import Dice
from rollsh.dice.notation import PrettyPrinter
from rollsh.dice.notation import roll
