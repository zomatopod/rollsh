import collections
import random
import operator
import functools
from abc import ABCMeta, abstractmethod

from grako.exceptions import ParseError

from rollsh.dice import base



###############################################################################
# Notation Parser #############################################################
###############################################################################

class Parser(base.Parser):
    ROOT_RULE = "dice_expression"

    def __init__(self):
        super().__init__(semantics=Semantics())

    def parse(self, text):
        return super().parse(text, self.ROOT_RULE)


class Semantics(base.Semantics):
    def dice_expression(self, ast):
        assert isinstance(ast, Arithmetic)
        return ast

    def number(self, ast):
        assert isinstance(ast, str)
        return Number(ast)

    def arithmetic(self, ast):
        return binop_tree(ast, Arithmetic, operand_class=Term)

    def term(self, ast):
        return binop_tree(ast, Term, operand_class=Factor)

    def factor(self, ast):
        assert isinstance(ast, (Atom, Arithmetic))
        return Factor(ast)

    def atom(self, ast):
        assert isinstance(ast, (Scalar, Roll))
        return Atom(ast)

    def scalar(self, ast):
        assert isinstance(ast, Number)
        return Scalar(ast)

    def roll(self, ast):
        amount, sides = ast['amount'], ast['sides']
        assert isinstance(amount, Number) or amount is None
        assert isinstance(sides, Number) or sides is None
        return Roll(amount, sides)

    def add_op(self, ast):
        return Operator(operator.add)

    def sub_op(self, ast):
        return Operator(operator.sub)

    def mul_op(self, ast):
        return Operator(operator.mul)

    def div_op(self, ast):
        return Operator(operator.floordiv)


class Operator(object):
    repr_symbols = dict(
        add="+",
        sub="-",
        mul="x",
        floordiv="/"
    )

    def __init__(self, operator_func, name=None):
        self.operator_func = operator_func
        self.name = name
        if self.name is None:
            self.name = operator_func.__name__

    def __call__(self, left, right):
        return self.operator_func(left, right)

    def __repr__(self):
        return self.repr_symbols.get(self.name, self.name)


def binop_tree(ast, binop_class, *, operand_class=None):
    """Helper utility to create a binary expression tree from the
    *ast* of a binary operation rule in which any arbitrary number
    of operands and operators may be parsed (e.g. "1+2+4-6+10"). The
    expression is evaluated from left-to-right when
    :meth:`BinaryOp.eval` is called on the returned root node.

    *binop_class* is the :class:`BinaryOp` class object to
    instantiate the nodes as.

    The optional *operand_class* keyword is used to assert the class
    type of the operands.
    """

    left_operand = ast['left_operand']
    operators = ast['operators']
    right_operands = ast['right_operands']
    if operand_class:
        # Operators should be
        assert isinstance(left_operand, operand_class)
        assert (all((isinstance(op, Operator) for op in operators)) or
                operators is None)
        assert (all((isinstance(o, operand_class) for o in right_operands)) or
                right_operands is None)
    if right_operands is None:
        return left_operand
    upper_node = left_operand
    for op, operand in zip(operators, right_operands):
        upper_node = binop_class(upper_node, op, operand)
    return upper_node



###############################################################################
# Abstract Syntax Tree Nodes ##################################################
###############################################################################

class AST(object):
    pass


class Number(AST):
    def __init__(self, number):
        self.integer = int(number)

    def __int__(self):
        return self.integer

    def __repr__(self):
        return str(self.integer)


class Expression(AST, metaclass=ABCMeta):
    @abstractmethod
    def eval(self, **kwargs):
        pass


class BinaryOp(Expression):
    def __init__(self, left, operator, right):
        self.left = left
        self.operator = operator
        self.right = right

    def eval(self, **kwargs):
        left = int(self.left.eval(**kwargs))
        right = int(self.right.eval(**kwargs))
        return self.operator(left, right)

    def __repr__(self):
        return "%s%s%s" % (self.left, self.operator, self.right)


class Arithmetic(BinaryOp):
    pass


class Term(Arithmetic):
    pass


class Factor(Term):
    def __init__(self, expr):
        self.expr = expr

    def eval(self, **kwargs):
        return self.expr.eval(**kwargs)

    def __repr__(self):
        if isinstance(self.expr, Arithmetic):
            return "(%s)" % self.expr
        return str(self.expr)


class Atom(Expression):
    def __init__(self, value):
        self.value = value

    def eval(self, **kwargs):
        return self.value.eval(**kwargs)

    def __repr__(self):
        return str(self.value)


class Scalar(Expression, Number):
    def eval(self, **kwargs):
        return int(self)


class Roll(Expression):
    class Result(list):
        def __init__(self, iter_values):
            self.extend(iter_values)

        def __int__(self):
            return sum(self)

    def __init__(self, amount, sides):
        self.amount = int(amount) if amount is not None else 1
        self.sides = int(sides) if sides is not None else 6
        assert self.amount > 0
        assert self.sides > 1

    def eval(self, **kwargs):
        func = random.randint
        if "range" in kwargs:
            if kwargs['range'] == "min":
                func = lambda a, b: 1
            if kwargs['range'] == "max":
                func = lambda a, b: self.sides
        result = self.Result(
            [func(1, self.sides) for _ in range(self.amount)]
        )
        if "roll_callback" in kwargs and kwargs['roll_callback'] is not None:
            kwargs['roll_callback'](self, result)
        return result

    def __repr__(self):
        return "%sd%s" % (self.amount, self.sides)



###############################################################################
# Utility #####################################################################
###############################################################################

class RollRecord(object):
    def __init__(self):
        # Holds the actual results of the roll, with the Roll node as the key
        # and its results list as the value.
        self.results = {}
        # A lookup of the Roll nodes that have been recorded, in order that
        # they have been evaluated.
        self.roll_order = []

    def __getitem__(self, index):
        # Can index by a node instance or int.
        try:
            # Try to index by node instance
            return self.results[index]
        except KeyError:
            # Try to index by roll order
            node = self.roll_order[index]
            return self.results[node]

    def __call__(self, roll_node, result):
        # To be invoked by the roll_callback option for AST evaluation.
        # See Roll.eval().
        self.results[roll_node] = result
        self.roll_order.append(roll_node)

    def __repr__(self):
        return repr(self.results)


class PrettyPrinter(object):
    def __init__(self):
        self.roll_record = {}

    def str(self, ast, roll_record):
        self.roll_record = roll_record
        return self.visit(ast)

    def visit(self, node):
        fn_name = "visit_" + node.__class__.__name__
        return getattr(self, fn_name)(node)

    def visit_Arithmetic(self, node):
        operator = node.operator
        lres = self.visit(node.left)
        rres = self.visit(node.right)
        return "%s %s %s" % (lres, operator, rres)

    def visit_Term(self, node):
        return self.visit_Arithmetic(node)

    def visit_Factor(self, node):
        expr = node.expr
        rexpr = self.visit(expr)
        if isinstance(expr, Arithmetic):
            return "(%s)" % rexpr
        return rexpr

    def visit_Atom(self, node):
        value = node.value
        return self.visit(value)

    def visit_Scalar(self, node):
        integer = node.integer
        return str(integer)

    def visit_Roll(self, node):
        amount = node.amount
        sides = node.sides
        try:
            result = self.roll_record[node]
            return "%sd%s%s" % (amount, sides, result)
        except KeyError:
            return "%sd%s" % (amount, sides)



###############################################################################
# Dice API ####################################################################
###############################################################################

class Dice(object):
    """A class that represents a dice *notation*, given as a string,
    and performs simulated rolls::

        >>> dice = Dice("2d6")
        >>> dice.roll()
        [1, 5]
        >>> dice.notation = "2d6+10"
        >>> dice.roll()
        21

    If the notation is invalid or otherwise unrecognized,
    :exc:`ValueError` is raised.
    """

    _parser = Parser()

    def __init__(self, notation="1d6"):
        self.ast = None
        self.notation = notation

    @property
    def notation(self):
        """The notation this :class:`Dice` represents. Setting this
        property will re-parse a new notation for future rolls.
        """

        return str(self.ast)

    @notation.setter
    def notation(self, notation):
        try:
            self.ast = self._parser.parse(notation)
        except ParseError:
            raise ValueError("%s is not a recognized dice notation" % notation)

    @property
    def max(self):
        """Read-only property of the maximum value possible for the
        represented notation. (Given ``2d6+10`` the maximum is ``22``.)
        """

        return int(self.ast.eval(range="max"))

    @property
    def min(self):
        """Read-only property of the minimum value possible for the
        represented notation. (Given ``2d6+10``, the minimum is ``12``.)
        """

        return int(self.ast.eval(range="min"))

    def roll(self, *, record_rolls=False):
        """Calculate a random roll and return the result.

        The result of a bare dice roll, without arithmetic, is given as
        a list of integers for each die that participated in the roll.
        For example, the roll for ``3d6`` will produce a result in the
        form of ``[5, 2, 4]``, which may then be summed using
        :func:`sum` or :func:`int`.

        For rolls that involve any sort of arithmetic operation, the
        result is always a summed integer.
        """

        record = RollRecord()
        result = self.ast.eval(roll_callback=record)
        if record_rolls:
            return result, record
        return result

    def __str__(self):
        return self.notation

    def __repr__(self):
        return "Dice(%s)" % self.notation


def roll(notation, *, get_instance=False, **options):
    """Utility function to perform a single roll of a *notation*. Refer
    to :meth:`Dice.roll` for return behavior and options.

    If *get_instance* is ``True``, then the function will also return the
    :class:`Dice` instance used to perform the roll as the last element of the
    return tuple.
    """

    inst = _get_dice(notation)
    res = inst.roll(**options)
    # Roll.roll() may return a single value or a tuple, and we need it to be
    # a tuple if get_instance is set.
    if get_instance and not isinstance(res, tuple):
        res = (res,)
    return res + (inst,) if get_instance else res


@functools.lru_cache()
def _get_dice(notation):
    return Dice(notation)
