import cmd

from rollsh import dice


class RollSh(cmd.Cmd):
    prompt = "> "
    def __init__(self):
        super().__init__()
        self.pprint = dice.PrettyPrinter()
        self.last_notation = ""
        self.stop = False

    def do_roll(self, notation):
        if not notation:
            print("*** No notation entered or saved")
            return
        try:
            result, rolls, instance = dice.roll(
                notation, record_rolls=True, get_instance=True
            )
        except ValueError:
            print("*** Could not parse the dice notation '%s'" % notation)
            return
        self.last_notation = notation
        print(self.pprint.str(instance.ast, rolls))
        print(int(result))

    def do_exit(self, line):
        self.stop = True

    def default(self, line):
        self.do_roll(line)

    def emptyline(self):
        self.default(self.last_notation)

    def postcmd(self, stop, line):
        if self.stop:
            return True


def run():
    RollSh().cmdloop()
